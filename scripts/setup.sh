#!/bin/bash

#-------------------------------------------------------------------------------------------------------------------------------
# FUNCTIONS
#-------------------------------------------------------------------------------------------------------------------------------

function check_root {
    if [[ $EUID -ne 0 ]]; then
        echo
        echo "This script must be run as root !"
        echo
        exit 1
    fi
}

function show_running_task {
    TASK=$1
    GUM_INSTALLED=$(which gum)
    if [ ! -z $GUM_INSTALLED ]; then
        gum style \
            --border normal --align center --width 50 --padding "1 1" \
            "$TASK"
    else
        echo --------------------------------------------------------------------------------------
        echo $TASK
        echo --------------------------------------------------------------------------------------
    fi
}

function show_distribution {
    DISTRIBUTION=$((lsb_release -ds || cat /etc/*release || uname -om) 2>/dev/null | head -n1  | cut -d "\"" -f 2)
    gum style \
        --border double --align center --width 50 --padding "2 4" \
        "$DISTRIBUTION"
}

function write_to_bashrc_module {
    MODULE=$1.bashrc
    CONTENT=$2
    if [ ! -f /home/$USER/.bashrc.d/$MODULE ]; then
        sudo -u $USER touch /home/$USER/.bashrc.d/$MODULE
    fi
    echo $CONTENT >> /home/$USER/.bashrc.d/$MODULE
}

#-------------------------------------------------------------------------------------------------------------------------------

function update {
    show_running_task "Updating system..."
    apt-get update
    apt-get upgrade -y
    apt-get autoremove -y
}

function install_tools {
    show_running_task "Installing tools..."
    apt-get update
    apt-get -y  install curl wget gpg
}

function install_gum {
    ALREADY_INSTALLED=$(which gum)
    if [ -z $ALREADY_INSTALLED ]; then
        show_running_task "Installing gum..."
        mkdir -p /etc/apt/keyrings
        curl -fsSL https://repo.charm.sh/apt/gpg.key | sudo gpg --dearmor > /etc/apt/keyrings/charm.gpg
        echo "deb [signed-by=/etc/apt/keyrings/charm.gpg] https://repo.charm.sh/apt/ * *" | sudo tee /etc/apt/sources.list.d/charm.list
        apt-get update
        apt-get -y  install gum
    fi
}

function install_screenfetch {
    ALREADY_INSTALLED=$(which screenfetch)
    if [ -z $ALREADY_INSTALLED ]; then
        show_running_task "Installing screenfetch"
        apt install -y screenfetch
        write_to_bashrc_module screenfetch "screenfetch"
        write_to_bashrc_module screenfetch "echo"
    fi
}

function install_oh_my_posh {
    ALREADY_INSTALLED=$(which oh-my-posh)
    if [ -z $ALREADY_INSTALLED ]; then
        show_running_task "Installing oh-my-posh"
        apt install -y zip unzip
        wget -q https://github.com/JanDeDobbeleer/oh-my-posh/releases/latest/download/posh-linux-amd64 -O /usr/local/bin/oh-my-posh
        chmod +x /usr/local/bin/oh-my-posh
        mkdir -p /home/$USER/.poshthemes
        wget -q https://github.com/JanDeDobbeleer/oh-my-posh/releases/latest/download/themes.zip -O /home/$USER/.poshthemes/themes.zip
        unzip -q -o /home/$USER/.poshthemes/themes.zip -d /home/$USER/.poshthemes
        chmod u+rw /home/$USER/.poshthemes/*.json
        rm /home/$USER/.poshthemes/themes.zip
        chown -R $USER: /home/$USER/.poshthemes
        write_to_bashrc_module ohmyposh 'eval "$(oh-my-posh init bash --config $HOME/.poshthemes/aliens.omp.json)"'
    fi
}

function install_jq {
    ALREADY_INSTALLED=$(which jqx)
    if [ -z $ALREADY_INSTALLED ]; then
        show_running_task "Installing jq"
        apt install -y jq
    fi
}

function create_user {
    show_running_task "Creating new user"
    USER=$(gum input --placeholder "User")
    echo "User"
    echo "> $USER"
    if id -u "$USER" >/dev/null 2>&1; then
        echo User already exists
        return
    fi
    PASSWORD=$(gum input --password --placeholder "Password")
    PASSWORD_AGAIN=$(gum input --password --placeholder "Password (again)")
    if [ "$PASSWORD" !=  "$PASSWORD_AGAIN" ]; then
        echo 'Passwords do not match.' | gum format -t emoji
        echo
        exit
    fi
    CYPHERED_PASSWORD=$(openssl passwd -6 -salt xyz  $PASSWORD)

    useradd --create-home --home-dir /home/$USER --password $CYPHERED_PASSWORD --shell /bin/bash $USER
    usermod -aG sudo $USER

    echo [user] >> /etc/wsl.conf
    echo default=$USER >> /etc/wsl.conf
    echo [automount] >> /etc/wsl.conf
    echo options = "metadata,umask=22,fmask=11" >> /etc/wsl.conf
    sed -i 's/systemd=true/systemd=false/g' /etc/wsl.conf

    sudo -u $USER mkdir -p /home/$USER/.bashrc.d
    echo "" >> /home/$USER/.bashrc
    echo '# Source all .bashrc files' >> /home/$USER/.bashrc
    echo 'if [ -d "$HOME/.bashrc.d" ]; then' >> /home/$USER/.bashrc
    echo '    for file in $HOME/.bashrc.d/*.bashrc; do' >> /home/$USER/.bashrc
    echo '        [ -r "$file" ] && [ -f "$file" ] && source "$file"' >> /home/$USER/.bashrc
    echo '    done' >> /home/$USER/.bashrc
    echo 'fi' >> /home/$USER/.bashrc
}

# function configure_python {
#     show_running_task 'Configuring python'
#     apt install -y pipx
#     sudo -u $USERNAME pipx ensurepath > /dev/null
#     source <(sudo -u $USERNAME cat /home/$USERNAME/.bashrc) > /dev/null
#     sudo -u $USERNAME PATH="$PATH:/home/$USERNAME/.local/bin" pipx install jinja2-cli
# }

function configure_wsl {
    show_running_task "Configuring wsl"
    TEMP_FILE=$(mktemp)
    case "$RELEASE_NAME" in
        "ubuntu") ICON="https://assets.ubuntu.com/v1/49a1a858-favicon-32x32.png"
        ;;
    esac
    cat $WSL_SETTINGS | tee $TEMP_FILE > /dev/null
    cat $TEMP_FILE | jq --arg NAME ${WSL_DISTRO_NAME} --arg LOGO ${ICON} '.profiles.list |= map(if .name == $NAME then . + {"icon": $LOGO} else . end)' | tee $TEMP_FILE  > /dev/null
    cat $TEMP_FILE | jq --arg NAME ${WSL_DISTRO_NAME} '.profiles.list |= map(if .name == $NAME then . + {"font": {"face": "FiraCode NF"}} else . end)' | tee $TEMP_FILE  > /dev/null
    cat $TEMP_FILE | jq --arg NAME ${WSL_DISTRO_NAME} '.profiles.list |= map(if .name == $NAME then . + {"colorScheme": "Solarized Dark"} else . end)' | tee $TEMP_FILE  > /dev/null
    mv $TEMP_FILE $WSL_SETTINGS
    echo Done.
}

#-------------------------------------------------------------------------------------------------------------------------------
# MAIN
#-------------------------------------------------------------------------------------------------------------------------------

export FOREGROUND="#61AFEF"
export BORDER_FOREGROUND="#61AFEF"
export GUM_INPUT_CURSOR_FOREGROUND="#61AFEF"

export WSL_SETTINGS=/mnt/c/Users/frede/AppData/Local/Packages/Microsoft.WindowsTerminal_8wekyb3d8bbwe/LocalState/settings.json

WSL_DISTRO_NAME=$1
RELEASE_NAME=$(cat /etc/*-release | grep ^ID= | awk -F'=' '{print $2}')

check_root

echo
update
install_tools
install_gum

echo
show_distribution

create_user

install_screenfetch
install_oh_my_posh
install_jq

# configure_python
configure_wsl

show_running_task "Terminated"
