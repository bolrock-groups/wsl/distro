#!/bin/bash

#-------------------------------------------------------------------------------------------------------------------------------
# FUNCTIONS
#-------------------------------------------------------------------------------------------------------------------------------

function check_root {
    if [[ $EUID -ne 0 ]]; then
        echo
        echo "This script must be run as root !"
        echo
        exit 1
    fi
}

function show_running_task {
    TASK=$1
    GUM_INSTALLED=$(which gum)
    if [ ! -z $GUM_INSTALLED ]; then
        gum style \
            --border normal --align center --width 50 --padding "1 1" \
            "$TASK"
    else
        echo --------------------------------------------------------------------------------------
        echo $TASK
        echo --------------------------------------------------------------------------------------
    fi
}

function show_distribution {
    DISTRIBUTION=$((lsb_release -ds || cat /etc/*release || uname -om) 2>/dev/null | head -n1  | cut -d "\"" -f 2)
    gum style \
        --border double --align center --width 50 --padding "2 4" \
        "$DISTRIBUTION"
}

function write_to_bashrc_module {
    MODULE=$1.bashrc
    CONTENT=$2
    if [ ! -f /home/$USER/.bashrc.d/$MODULE ]; then
        sudo -u $USER touch /home/$USER/.bashrc.d/$MODULE
    fi
    echo $CONTENT >> /home/$USER/.bashrc.d/$MODULE
}

#-------------------------------------------------------------------------------------------------------------------------------

function select_packages {
    show_running_task "Package(s) to install"
    SELECTED_PACKAGES=$(gum choose --no-limit $AVAILABLE_PACKAGES | awk '{print tolower($0)}')
    readarray -d ' ' SELECTED_PACKAGES_ARRAY <<< $SELECTED_PACKAGES
    for package in ${SELECTED_PACKAGES_ARRAY[@]}; do
        echo "> $package"
    done
    echo
    for package in ${SELECTED_PACKAGES_ARRAY[@]}; do
        install_package $package
    done
    echo
}

function install_package {
    PACKAGE=$1
    case $PACKAGE in
        ansible)
            install_ansible
            ;;
        docker)
            install_docker
            ;;
        vagrant)
            install_vagrant
            ;;
        *)
            echo Unkown package [$PACKAGE]
            exit 1
            ;;
    esac
}

function install_docker {
    show_running_task "Installing docker"

    # Add Docker's official GPG key:
    apt-get update
    apt-get install ca-certificates curl gnupg
    install -m 0755 -d /etc/apt/keyrings
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor --yes -o /etc/apt/keyrings/docker.gpg
    chmod a+r /etc/apt/keyrings/docker.gpg

    if [[ $RELEASE_NAME -eq "debian" ]]; then
        URL=https://download.docker.com/linux/debian
    else
        URL=https://download.docker.com/linux/ubuntu
    fi

    # Add the repository to Apt sources:
    echo \
    "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] $URL \
    "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
    tee /etc/apt/sources.list.d/docker.list > /dev/null
    apt-get update

    apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
    groupadd -f docker
    usermod -aG docker $USER

    echo "$USER ALL=(ALL) NOPASSWD: /usr/sbin/service docker *" > /etc/sudoers.d/docker
    echo "#!/bin/bash" > /home/$USER/start-docker.sh
    echo "sudo service docker start" >> /home/$USER/start-docker.sh
    chown $USER: /home/$USER/start-docker.sh
    chmod +x /home/$USER/start-docker.sh
    ln -s /home/$USER/start-docker.sh /usr/sbin/start-docker

    echo ----------------------------------------------------
    sudo -u $USER docker -v
    sudo -u $USER docker compose version
}

function install_ansible {
    show_running_task "Installing ansible"
    apt update
    apt -y install ansible sshpass

    echo ----------------------------------------------------
    sudo -u $USER ansible --version
}

function install_vagrant {
    show_running_task "Installing vagrant"
    wget -q -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
    echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/hashicorp.list
    apt update
    apt -y install vagrant

    write_to_bashrc_module vagrant 'export VAGRANT_WSL_ENABLE_WINDOWS_ACCESS="1"'

    echo ----------------------------------------------------
    sudo -u $USER vagrant --version
}

#-------------------------------------------------------------------------------------------------------------------------------
# MAIN
#-------------------------------------------------------------------------------------------------------------------------------

export FOREGROUND="#61AFEF"
export BORDER_FOREGROUND="#61AFEF"
export GUM_INPUT_CURSOR_FOREGROUND="#61AFEF"

export  AVAILABLE_PACKAGES="docker ansible vagrant"

RELEASE_NAME=$(cat /etc/*-release | grep ^ID= | awk -F'=' '{print $2}')
USER=$(cat /etc/wsl.conf | grep default | awk 'BEGIN {FS="=";} { print $2 }')

check_root

echo
show_distribution

select_packages