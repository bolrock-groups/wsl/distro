$DISTRIBUTION_FOLDER = "$Env:DEV_DATA_HOME/wsl_distributions"
$WSL_SETTINGS="C:\Users\frede\AppData\Local\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState\settings.json"
$WSL_STATE="C:\Users\frede\AppData\Local\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState\state.json"

####################################################################################################################################

function Show-Usage {
    Write-Host
    Write-Host "Usage : distro <command>"
    Write-Host
    Write-Host "   Commands :"
    Write-Host
    Write-Host "        list"
    Write-Host "        pull <distribution>"
    Write-Host "        create <distribution> <target>"
    Write-Host "        destroy <target>"
    Write-Host
    exit 0
}

####################################################################################################################################

function Check-Dev-Config {
    if (-not (Get-Item "Env:DEV_HOME" -ErrorAction SilentlyContinue)) {
        Write-Host
        Write-host -f Red "Environment variable DEV_HOME does not exists."
        Write-Host
        exit 1
    }
    if (-not (Get-Item "Env:DEV_DATA_HOME" -ErrorAction SilentlyContinue)) {
        Write-Host
        Write-host -f Red "Environment variable DEV_DATA_HOME does not exists."
        Write-Host
        exit 1
    }
}
function Check-Distributions-Folder {
    if (-not (Test-Path $DISTRIBUTION_FOLDER)) { New-Item -ItemType Directory -Path $DISTRIBUTION_FOLDER | Out-Null }
}

####################################################################################################################################

function Display-All-Distributions {
    Check-Distributions-Folder
    Write-Host
    Display-Wsl-Distributions
    Write-Host
    Display-Pulled-Distributions
    Write-Host
    Display-Pullable-Distributions
    Write-Host
}
function Display-Wsl-Distributions {
    $distributionNames = Get-Wsl-Distributions
    if ($distributionNames.Length -ne 0) {
        Write-Host Wsl distributions
        Write-Host -----------------
        $distributionNames
    }
}
function Display-Pulled-Distributions {
    $distributionNames = Get-Pulled-Distributions
    if ($distributionNames.Length -ne 0) {
        Write-Host Pulled distributions
        Write-Host --------------------
        $distributionNames
    }
}
function Display-Pullable-Distributions {
    $distributionNames = Get-Pullable-Distributions
    if ($distributionNames.Length -ne 0) {
        Write-Host Pullable distributions
        Write-Host ----------------------
        $distributionNames
    }
}
function Get-Pullable-Distributions {
    $onlineDistributions = (wsl -l --online) | Where-Object { $_.Trim() -ne '' }
    $nameIndex = $onlineDistributions.IndexOf(($onlineDistributions -like "NAME*")[0])
    $nameIndex++
    $onlineDistributions = $onlineDistributions[$nameIndex..$onlineDistributions.Length]
    return $onlineDistributions | Select-String -Pattern '^(\S+)' | ForEach-Object { $_.Matches.Groups[1].Value } | ForEach-Object { $_ -replace '"', '' }
}
function Get-Pulled-Distributions {
    If ((Get-ChildItem "$DISTRIBUTION_FOLDER\*.tar.gz" -Force | Measure-Object).Count -eq 0) { return "" }
    return Get-ChildItem "$DISTRIBUTION_FOLDER\*.tar.gz" | ForEach-Object { $_.BaseName -replace '\.tar$', '' }
}
function Get-Wsl-Distributions {
    $wslDistributions = (wsl -l -v) | Where-Object { $_.Trim() -ne '' }
    $windowsIndex = $wslDistributions.IndexOf(($wslDistributions -like "Windows*")[0])
    $windowsIndex++
    return $wslDistributions[$windowsIndex..$wslDistributions.Length]
}

####################################################################################################################################

function Pull-Distribution {
    param ([string]$DISTRIBUTION_NAME)
    if (-not $DISTRIBUTION_NAME) {
        Write-Host
        Write-Host "Usage: distro pull <distribution>"
        Write-Host
        exit 1
    }
    Check-Distributions-Folder
    $distributionNames = Get-Pullable-Distributions
    if ($distributionNames -notcontains $DISTRIBUTION_NAME) {
        Write-Host
        Display-Pullable-Distributions
        Write-Host
        exit 1
    }
    $distributionNames = Get-Pulled-Distributions
    if ($distributionNames -contains $DISTRIBUTION_NAME) {
        Write-Host
        Display-Pulled-Distributions
        Write-Host
        exit 1
    }
    Write-Host
    Write-Host "Pulling distribution..."
    wsl --install --no-launch --distribution $DISTRIBUTION_NAME
    $SEARCHED_DISTRIBUTION_NAME = "*$DISTRIBUTION_NAME*" -replace '-', ''
    $DISTRIBUTION_PACKAGE=Get-AppxPackage | Where-Object { $_.Name -like $SEARCHED_DISTRIBUTION_NAME }
    $DISTRIBUTION_EXECUTABLE=(Get-Childitem -Path $DISTRIBUTION_PACKAGE.InstallLocation -Filter *.exe | Where-Object {$_.FullName -notMatch "setup"} | Select -Last 1).BaseName
    Invoke-Expression "$DISTRIBUTION_EXECUTABLE install --root"
    wsl --export $DISTRIBUTION_NAME "$DISTRIBUTION_FOLDER\$DISTRIBUTION_NAME.tar.gz"
    wsl --unregister $DISTRIBUTION_NAME
    Get-AppxPackage -Name $DISTRIBUTION_PACKAGE.name | Remove-AppxPackage
    Write-Host
    Display-Pulled-Distributions
    Write-Host
}

####################################################################################################################################

function Create-Wsl-Distribution {
    param ([string]$DISTRIBUTION_NAME, [string]$WSL_DISTRIBUTION)
    if (-not $DISTRIBUTION_NAME -or -not $WSL_DISTRIBUTION) {
        Write-Host
        Write-Host "Usage: distro create <distribution> <wsl-distribution-name>"
        Write-Host
        exit 1
    }
    Check-Distributions-Folder
    $distributionNames = Get-Pulled-Distributions
    if ($distributionNames -notcontains $DISTRIBUTION_NAME) {
        Write-Host
        Display-Pulled-Distributions
        Write-Host
        exit 1
    }
    if (Test-Path "$DISTRIBUTION_FOLDER\$WSL_DISTRIBUTION") {
        Write-Host
        Display-Wsl-Distributions
        Write-Host
        exit 1
    }
    Write-Host
    wsl --import $WSL_DISTRIBUTION "$DISTRIBUTION_FOLDER\$WSL_DISTRIBUTION" "$DISTRIBUTION_FOLDER\$DISTRIBUTION_NAME.tar.gz"
    Start-Process "wt.exe" -WindowStyle Hidden
    Start-Sleep -Seconds 2
    Get-Process | Where-Object { $_.ProcessName -eq "WindowsTerminal" } | ForEach-Object { Stop-Process -Id $_.Id }
    wsl -d $WSL_DISTRIBUTION ./scripts/setup.sh $WSL_DISTRIBUTION
    wsl -t $WSL_DISTRIBUTION
    Write-Host
    Display-Wsl-Distributions
    Write-Host
}

####################################################################################################################################

function Destroy-Instance {
    param ([string]$WSL_DISTRIBUTION)
    if (-not $WSL_DISTRIBUTION) {
        Write-Host
        Write-Host "Usage: distro destroy <wsl-distribution-name>"
        Write-Host
        exit 1
    }
    if (-not (Test-Path "$DISTRIBUTION_FOLDER\$WSL_DISTRIBUTION")) {
        Write-Host
        Display-Wsl-Distributions
        Write-Host
        exit 1
    }
    Check-Distributions-Folder
    Write-Host
    Get-Process | Where-Object { $_.ProcessName -eq "WindowsTerminal" } | ForEach-Object { Stop-Process -Id $_.Id }
    $settings = Get-Content $WSL_SETTINGS -Raw | ConvertFrom-Json
    $profile_guid = $settings.profiles.list | ForEach {
        $profile = $_
        if ($profile.name -eq $WSL_DISTRIBUTION) { return $profile.guid }
    }
    if ($profile_guid -ne $null) {
        $state = Get-Content $WSL_STATE -Raw | ConvertFrom-Json
        $state.generatedProfiles = $state.generatedProfiles -ne $profile_guid
        $state | ConvertTo-Json -Depth 100 | Set-Content -Path $WSL_STATE
        $settings.profiles.list = $settings.profiles.list | Where-Object { $_.guid -ne $profile_guid }
        $settings | ConvertTo-Json -Depth 100 | Set-Content -Path $WSL_SETTINGS
    } else {
        Write-Host Profile $WSL_DISTRIBUTION not found !!
        Write-Host
        exit 1
    }
    wsl --unregister $WSL_DISTRIBUTION
    Remove-Item -Path "$DISTRIBUTION_FOLDER\$WSL_DISTRIBUTION" -Recurse
    Write-Host
}

####################################################################################################################################

function Run-Extra {
    param([string]$WSL_DISTRIBUTION)
    if (-not $WSL_DISTRIBUTION) {
        Write-Host
        Write-Host "Usage: distro extra <wsl-distribution-name>"
        Write-Host
        exit 1
    }
    if (-not (Test-Path "$DISTRIBUTION_FOLDER\$WSL_DISTRIBUTION")) {
        Write-Host
        Display-Wsl-Distributions
        Write-Host
        exit 1
    }
    Check-Distributions-Folder
    wsl -d $WSL_DISTRIBUTION sudo ./scripts/extra.sh
}

####################################################################################################################################

Check-Dev-Config
$COMMAND=$args[0]
Switch ($COMMAND)
{
    "list" { Display-All-Distributions }
    "pull" { Pull-Distribution $args[1] }
    "create" { Create-Wsl-Distribution $args[1] $args[2] }
    "destroy" { Destroy-Instance $args[1] }
    "extra" { Run-Extra  $args[1] }
    Default { Show-Usage }
}
